
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->

</head>
<body>

<div class="container">

        @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <h4>{{$errors->first()}}</h4>
        </div>
        @endif

        <form action="{{ action('MainpageController@addzip') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="countries">Countries</label>
                <select id="countries" name="country" class="form-control">
                    <option selected value="">--Choose a Country--</option>
                    @foreach($countries as $country)
                    <option value="{!! $country->countryCode !!}">{!! $country->country !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="zip">ZIP Code</label>
                <input type="text" id="zip" name="zipcode" class="form-control">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

</div>
@if (\Session::has('success'))
    <div class="alert alert-success">
        <h4>We found this list of places</h4>
        <ul>
            @foreach(\Session::get('success')  as $place)
            <li>{!! $place->name !!}, {!! $place->place!!}, long = {!! $place->longitude!!}, lat = {!! $place->latitude !!}</li>
            @endforeach
        </ul>
    </div>
@endif
</body>
</html>
