<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
        ["country" => "Afghanistan","countryCode" => "AF"],
        ["country" => "Albania","countryCode" => "AL"],
        ["country" => "Algeria","countryCode" => "DZ"],
        ["country" => "American Samoa","countryCode" => "AS"],
        ["country" => "Andorra","countryCode" => "AD"],
        ["country" => "Angola","countryCode" => "AO"],
        ["country" => "Anguilla","countryCode" => "AI"],
        ["country" => "Antarctica","countryCode" => "AQ"],
        ["country" => "Antigua and Barbuda","countryCode" => "AG"],
        ["country" => "Argentina","countryCode" => "AR"],
        ["country" => "Armenia","countryCode" => "AM"],
        ["country" => "Aruba","countryCode" => "AW"],
        ["country" => "Australia","countryCode" => "AU"],
        ["country" => "Austria","countryCode" => "AT"],
        ["country" => "Azerbaijan","countryCode" => "AZ"],
        ["country" => "Bahamas","countryCode" => "BS"],
        ["country" => "Bahrain" ,"countryCode" => "BH"],
        ]);
    }
}
