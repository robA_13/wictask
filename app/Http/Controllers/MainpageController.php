<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Countries;
use App\Zipcodes;
use App\Zipinfo;
use Illuminate\Support\Facades\Validator;


class MainpageController extends Controller
{

    public function mainpage(){

        $countries = Countries::get();

        return view('mainpage', compact('countries'));

    }

    public function addzip(Request $request){
        $data = $request->except('_token');
        $validator = Validator::make($data,
            [
                'country' => 'required',
                'zipcode' => 'required'
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['All fields are required']);
        }

        $zipcode = trim($data['zipcode']);

        $checkforZip = Zipcodes::where(['zipcode'=>$zipcode])->get()->first();

        if(!$checkforZip){
            $getTheCountryCode = Countries::where(['countryCode'=>$data['country']])->get()->first();
            if($getTheCountryCode){
                $apiresponse = $this->getPlacesApi($getTheCountryCode->countryCode, $zipcode);

                if($apiresponse['status'] === 'ok' ){
                    $newZipCode = Zipcodes::create([
                        'country_id' => $getTheCountryCode->id,
                        'zipcode'    => $zipcode
                    ]);
                    if($newZipCode){
                        foreach ($apiresponse['result']['places'] as $place){
                            $arrayPlace = (array) $place;
                            Zipinfo::create([
                                'zipcode_id' => $newZipCode->id,
                                'place' => $arrayPlace['state'],
                                'name' => $arrayPlace['place name'],
                                'longitude' => $arrayPlace['longitude'],
                                'latitude' => $arrayPlace['latitude'],
                            ]);
                        }
                        return redirect('/')->with('success', $newZipCode->zipinfo);
                    }
                }else{
                    return redirect()->back()->withErrors(['No such country with entered zip code']);
                }
            }else{
                return redirect()->back()->withErrors(['No such country with entered zip code']);
            }
        }else{
            return redirect('/')->with('success', $checkforZip->zipinfo);
        }

    }

    public function getPlacesApi($countrycode,$zipcode){
        $ch = curl_init();// set url
        curl_setopt($ch, CURLOPT_URL, "api.zippopotam.us/".mb_strtolower($countrycode)."/".$zipcode."");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $resp = (array) json_decode($output);
        if(isset($resp['places']) && !empty($resp['places'])){
            return array('status'=>'ok','result'=>$resp);
        }else{
            return array('status'=>'fail','msg'=>'Not found');
        }
    }
}
