<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
     protected  $table = 'countries';

     protected  $fillable = ['country', 'countryCode'];

    public function zipcodes()
    {
        return $this->hasMany('App\Zipcodes');
    }

}
