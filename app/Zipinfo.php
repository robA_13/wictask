<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zipinfo extends Model
{
    protected  $table = 'zipinfo';

    protected  $fillable = ['zipcode_id','place','name','longitude','latitude'];

}
