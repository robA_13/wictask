<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zipcodes extends Model
{
    protected  $table = 'zipcodes';

    protected  $fillable = ['country_id','zipcode'];

    public function zipinfo()
    {
        return $this->hasMany('App\Zipinfo' , 'zipcode_id');
    }
}
